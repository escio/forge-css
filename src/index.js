'use strict';

const gulpAutoprefixer = require('gulp-autoprefixer');
const gulpIf = require('gulp-if');
const gulpSass = require('gulp-sass');
const gulpSourcemaps = require('gulp-sourcemaps');
const pipe = require('multipipe');

const compileSass = ({
	minimize = true,
	sourcemaps = true
}) => {
	return pipe(
		gulpIf(sourcemaps, gulpSourcemaps.init()),
		gulpSass({
			outputStyle: minimize ? 'compressed' : 'nested'
		}),
		gulpAutoprefixer({
			browsers: [
				'last 2 versions'
			],
			cascade: false
		}),
		gulpIf(sourcemaps, gulpSourcemaps.write('./'))
	);
};

module.exports = {
	compileSass
};
