# @escio/forge-css

> Best practice tools for forging css.

This package exposes a gulp/Vinyl transform stream that compiles SASS into CSS using the best practice config used internally at Norwegian company Escio AS. The idea is to provide good defaults without burdening the user with doing the config job.

## Installation

```
$ npm install @escio/forge-css
```

## Usage

```javascript
const gulp = require('gulp');
const forgeCss = require('@escio/forge-css');

gulp.task('build', () => gulp.src(
    '*.scss'
).pipe(
    forgeCss.compileSass({
        minimize: true, // Default
        sourcemaps: true // Default
    })
).pipe(
    gulp.dest('./build')
));
```

The resulting CSS is (optionally) minimized and sourcemapped, as well as auto-prefixed for compatibility with the 2 latest versions of all major browsers.

## License

ISC � Escio AS
